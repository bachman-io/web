<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->name('home');
Route::get('contact', 'PageController@contact')->name('contact');

Route::prefix('blog')->group(function () {
    Route::get('/', 'BlogController@index')->name('blog.index');
    Route::get('/category/{id}/{slug?}', 'BlogController@category')->name('blog.category');
    Route::get('/archive/{year}/{month}', 'BlogController@archive')->name('blog.archive');
    Route::get('/post/{id}/{slug?}', 'BlogController@post')->name('blog.post');
});

/*Route::prefix('stories')->group( function() {
    Route::get('/', 'StoriesController@index')->name('stories.index');
    Route::get('/genre/{id}/{slug?}', 'StoriesController@genre')->name('stories.genre');
    Route::get('/book/{story_id}/{chapter_id}/{story_slug?}/{chapter_slug?}',
        'StoriesController@book')->name('stories.book');
});*/

/*Route::get('/kitchen_sink', function () {
    return view('layout.kitchen_sink');
});*/

Route::prefix('admin')
    ->namespace('Admin')
    ->middleware('auth')
    ->group(function () {
        Route::get('/', 'AdminPageController@index');
        Route::resource('category', 'CategoryController', ['except' => ['show']]);
        Route::resource('post', 'PostController');
        Route::resource('genre', 'GenreController', ['except' => ['show']]);
        Route::resource('label', 'LabelController', ['except' => ['show']]);
        Route::resource('story', 'StoryController');
        Route::prefix('story/{story}')->group(function () {
            Route::resource('chapter', 'ChapterController');
            Route::resource('note', 'AuthorNoteController');
        });

        Route::get('/logout', 'AdminPageController@logout')->name('admin.logout');
});

Route::prefix('apiv1')
    ->namespace('API')
    ->group(function () {
        Route::get('twitch', 'TwitchController@index')->name('twitch');
    });

Auth::routes();
