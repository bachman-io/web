<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 50);
            $table->string('slug', 100);
            $table->boolean('published')->default(0);
            $table->enum('status', ['Complete', 'In Progress', 'Cancelled']);
            $table->enum('age_rating', ['Everyone', 'Teen', 'Adult']);
            $table->text('title_bg_image_url')->nullable();
            $table->text('synopsis');
            $table->unsignedInteger('author_id');
            $table->unsignedInteger('genre_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stories');
    }
}
