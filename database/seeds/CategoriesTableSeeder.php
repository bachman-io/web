<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => '!Announcements',
            'slug' => 'announcements',
        ]);

        DB::table('categories')->insert([
            'name' => 'Web Development',
            'slug' => 'web-development',
        ]);

        DB::table('categories')->insert([
            'name' => 'Anime',
            'slug' => 'anime',
        ]);

        DB::table('categories')->insert([
            'name' => 'Fiction',
            'slug' => 'fiction',
        ]);

        DB::table('categories')->insert([
            'name' => 'Movies',
            'slug' => 'movies',
        ]);
    }
}
