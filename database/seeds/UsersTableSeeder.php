<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Collin Bachman',
            'email' => 'collin@bachman.io',
            'password' => password_hash(env('DUMMY_USER_PASSWORD', 'hunter2'), PASSWORD_DEFAULT),
        ]);
    }
}
