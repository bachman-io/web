<?php

use Illuminate\Database\Seeder;
use App\Label;

class StoriesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Story::class, 10)
        ->create()
        ->each(
            function ($s) {
                $s->chapters()->save(
                    factory(App\Chapter::class)->make()
                );
                $label1 = Label::find(rand(1,3));
                $label2 = Label::find(rand(4,6));
                $label3 = Label::find(rand(7,9));
                $s->labels()->sync([
                    $label1->id,
                    $label2->id,
                    $label3->id
                ]);
                $s->notes()->save(
                    factory(App\AuthorNote::class)->make()
                );
            }
        );
    }
}
