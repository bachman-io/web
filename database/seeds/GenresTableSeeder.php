<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
            'name' => 'Adventure',
            'slug' => 'adventure',
        ]);

        DB::table('genres')->insert([
            'name' => 'Drama',
            'slug' => 'drama',
        ]);

        DB::table('genres')->insert([
            'name' => 'Romance',
            'slug' => 'romance',
        ]);

        DB::table('genres')->insert([
            'name' => 'Science Fiction',
            'slug' => 'science-fiction',
        ]);

        DB::table('genres')->insert([
            'name' => 'Non-fiction',
            'slug' => 'Non-fiction',
        ]);
    }
}
