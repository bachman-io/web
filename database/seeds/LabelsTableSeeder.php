<?php

use Illuminate\Database\Seeder;

class LabelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('labels')->insert([
            'name' => 'Fantasy Violence',
            'slug' => 'fantasy-violence',
        ]);

        DB::table('labels')->insert([
            'name' => 'Violence',
            'slug' => 'violence',
        ]);

        DB::table('labels')->insert([
            'name' => 'Blood and Gore',
            'slug' => 'blood-and-gore',
        ]);

        DB::table('labels')->insert([
            'name' => 'Suggestive Themes',
            'slug' => 'suggestive-themes',
        ]);

        DB::table('labels')->insert([
            'name' => 'Sexual Content',
            'slug' => 'sexual-content',
        ]);

        DB::table('labels')->insert([
            'name' => 'Horrific Sexual Content',
            'slug' => 'horrific-sexual-content',
        ]);

        DB::table('labels')->insert([
            'name' => 'Ridiculous',
            'slug' => 'ridiculous',
        ]);

        DB::table('labels')->insert([
            'name' => 'Funny',
            'slug' => 'funny',
        ]);

        DB::table('labels')->insert([
            'name' => 'Traumatic',
            'slug' => 'traumatic',
        ]);
    }
}
