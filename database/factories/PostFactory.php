<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    $bg_images = [
        null,
        $faker->imageUrl(1200, 300)
    ];
    return [
        'title' => $faker->text(50),
        'slug' => $faker->slug(5),
        'type' => 'Long',
        'published' => mt_rand(0, 1),
        'title_bg_image_url' => $bg_images[mt_rand(0, 1)],
        'author_id' => 1,
        'category_id' => mt_rand(1, 5),
        'lead_text' => $faker->paragraph(4),
        'main_text' => $faker->paragraphs(5, true),
        'created_at' => $created = $faker->dateTimeBetween('-5 years'),
        'updated_at' => $created,
    ];
});
