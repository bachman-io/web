<?php

use Faker\Generator as Faker;

$factory->define(App\Story::class, function (Faker $faker) {

    $bg_images = [
        null,
        $faker->imageUrl(1200, 300)
    ];

    $statuses = [
        'Complete',
        'In Progress',
        'Cancelled'
    ];

    $ageRatings = [
        'Everyone',
        'Teen',
        'Adult'
    ];

    return [
        'title' => $faker->text(50),
        'slug'=> $faker->slug(2),
        'published' => mt_rand(0, 1),
        'status' => $statuses[mt_rand(0, 2)],
        'age_rating' => $ageRatings[mt_rand(0, 2)],
        'title_bg_image_url' => $bg_images[mt_rand(0, 1)],
        'author_id' => 1,
        'genre_id' => mt_rand(1, 5),
        'synopsis' => $faker->paragraph(4),
        'created_at' => $created = $faker->dateTimeBetween('-5 years'),
        'updated_at' => $created,
    ];
});
