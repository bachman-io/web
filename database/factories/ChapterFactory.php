<?php

use Faker\Generator as Faker;

$factory->define(App\Chapter::class, function (Faker $faker) {
    return [
        'story_chapter_number' => rand(1,10),
        'title' => $faker->text(50),
        'slug' => $faker->slug(2),
        'published' => 1,
        'text' => $faker->paragraphs(10, true),
        'word_count' => mt_rand(1000, 50000),
        'created_at' => $created = $faker->dateTimeBetween('-5 years'),
        'updated_at' => $created,
    ];
});
