<?php

use Faker\Generator as Faker;
use App\AuthorNote;

$factory->define(AuthorNote::class, function (Faker $faker) {
    return [
        'text' => $faker->paragraph(),
        'created_at' => $created = $faker->dateTimeBetween('-5 years'),
        'updated_at' => $created,
    ];
});
