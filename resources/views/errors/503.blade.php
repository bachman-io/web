<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Some guy on the internet publishes some stores.">
    <meta name="author" content="Collin Bachman">
    <link rel="icon" href="favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>503 We'll Be Right Back - Bachman I/O</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/app.css" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/core.js" integrity="sha256-YCbKJH6u4siPpUlk130udu/JepdKVpXjdEyzje+z1pE=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">


</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background: #666;">
        <div class="container">
            <div class="links mx-auto">
                <a href="{{ route('home') }}">
                    <img src="{{ env('CDN_URL') }}/bachman-io-logo.svg" width="270" height="30" alt="Bachman I/O">
                </a>
            </div>
        </div>
</nav>

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4 text-center">♪ After these messages we'll be right back! ♪</h1>
        <p class="lead text-center">We weren't able to serve the page you were looking for, because the site is either under maintenance (very likely) or experiencing heavy load (unlikely). We should be back online in a few minutes.</p>
    </div>
</div>

<hr>
<footer class="py-5">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy;  Bachman I/O</p>
    </div>
</footer>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script type="text/javascript" src="markitup/jquery.markitup.js"></script>

</body>
</html>