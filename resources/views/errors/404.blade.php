@extends('layout.default')
@section('content')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4 text-center">Alas, you have reached a dead end.</h1>
            <p class="lead text-center">We weren't able to find the page you were looking for. It's likely been deleted, or I moved it somewhere else and didn't do redirection properly. Either way, this has been logged for my eventual review. Sorry for the inconvenience!</p>
        </div>
    </div>
@endsection