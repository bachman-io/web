@extends('layout.default')
@section('content')
    <div class="container">
        <div class="row">

            @include('blog.sidebar')

            <!-- Blog Entries Column -->
            <div class="col-md-8">
                <!-- Blog Post -->
                @forelse($posts as $post)
                    @switch($post->type)
                        @case('Short')
                        <div class="card mb-4">
                            <div class="card-body">
                                <h2 class="card-title">{{ $post->lead_text }}</h2>
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('blog.post', ['id' => $post->id, 'slug' => $post->slug]) }}" class="btn btn-primary">Permalink &rarr;</a>
                                <hr>
                                Posted {{ $postDates[$post->id]['created'] }}<br>
                                @if($post->created_at->format('ymd') < $post->updated_at->format('ymd'))
                                    Last Updated {{ $postDates[$post->id]['updated'] }}<br>
                                @endif
                                by {{ $post->author->name }}<br>
                                in <a href="{{ route('blog.category', ['id' => $post->category->id, 'slug' => $post->category->slug]) }}">{{ $post->category->name }}</a>
                            </div>
                        </div>
                    @break
                    @case('Embed')
                        <div class="card mb-4">
                            <div class="card-body">
                                @if($post->nsfw === 1)
                                <h2 class="card-title">{{ '[NSFW] ' . $post->title }}</h2>
                                @else
                                <h2 class="card-title">{{ $post->title }}</h2>
                                @endif
                                <p class="card-body"><em>This post contains embedded media. It's hidden so as to not take up a ton of space.</em></p>
                            </div>
                            <div class="card-footer">
                                @if($post->nsfw === 1)
                                    <a href="{{ route('blog.post', ['id' => $post->id, 'slug' => $post->slug]) }}" class="btn btn-danger">Show Embed [NSFW] &rarr;</a>
                                @else
                                    <a href="{{ route('blog.post', ['id' => $post->id, 'slug' => $post->slug]) }}" class="btn btn-primary">Show Embed &rarr;</a>
                                @endif
                                <hr>
                                Posted {{ $postDates[$post->id]['created'] }}<br>
                                @if($post->created_at->format('ymd') < $post->updated_at->format('ymd'))
                                    Last Updated {{ $postDates[$post->id]['updated'] }}<br>
                                @endif
                                by {{ $post->author->name }}<br>
                                in <a href="{{ route('blog.category', ['id' => $post->category->id, 'slug' => $post->category->slug]) }}">{{ $post->category->name }}</a>
                            </div>
                        </div>
                    @break
                        @case('Long')
                    <div class="card mb-4">
                        @if(!is_null($post->title_bg_image_url))
                            <img class="card-img-top" src="{{ $post->title_bg_image_url }}" alt="{{ $post->title }}">
                        @endif
                        <div class="card-body">
                            @if(is_null($post->title_bg_image_url))
                                @if($post->nsfw === 1)
                                    <h2 class="card-title">{{ '[NSFW] ' . $post->title }}</h2>
                                @else
                                    <h2 class="card-title">{{ $post->title }}</h2>
                                @endif
                            @endif
                            <p class="card-text">{{ $post->lead_text }}</p>
                        </div>
                        <div class="card-footer">
                            @if ($post->nsfw === 1)
                                <a href="{{ route('blog.post', ['id' => $post->id, 'slug' => $post->slug]) }}" class="btn btn-danger">Read More [NSFW] &rarr;</a>
                            @else
                                <a href="{{ route('blog.post', ['id' => $post->id, 'slug' => $post->slug]) }}" class="btn btn-primary">Read More &rarr;</a>
                            @endif
                            <hr>
                            Posted {{ $postDates[$post->id]['created'] }}<br>
                            @if($post->created_at->format('ymd') < $post->updated_at->format('ymd'))
                                Last Updated {{ $postDates[$post->id]['updated'] }}<br>
                            @endif
                            by {{ $post->author->name }}<br>
                            in <a href="{{ route('blog.category', ['id' => $post->category->id, 'slug' => $post->category->slug]) }}">{{ $post->category->name }}</a>
                        </div>
                    </div>
                    @break
                    @endswitch
                @empty
                    <h2>Maybe someday there will be blog posts here, but alas, there aren't any here now...</h2>
                @endforelse


                {{ $posts->links() }}

            </div>

        </div>
        <div class="row">
            <div class="col-md-12 d-md-none">
                <div class="card my-4">
                    <h5 class="card-header">Categories</h5>
                    <div class="card-body">
                        <ul class="list-inline mb-0">
                            @forelse($categories as $c)
                                <li class="list-inline-item"><a href="{{ route('blog.category', ['id' => $c->id, 'slug' => $c->slug]) }}">{{ $c->name }} <span class="badge badge-secondary">{{ $c->posts_count }}</span></a></li>
                            @empty
                                <li>There are no categories to display!</li>
                            @endforelse
                        </ul>
                    </div>
                </div>
                <div class="card my-4">
                    <h5 class="card-header">Archive</h5>
                    <div class="card-body">
                        @forelse($archive_months as $year2 => $month2)
                            <h3>{{ $year2 }}</h3>
                            <ul class="list-inline mb-0">
                                @foreach($month2 as $m2)
                                    <li class="list-inline-item">
                                        <a href="{{ route('blog.archive', ['year' => $year2, 'month' => $m2->month]) }}">{{ $m2->month_name }} <span class="badge badge-secondary">{{ $m2->posts_count }}</span></a>
                                    </li>
                                @endforeach
                            </ul>
                        @empty
                            There are no archives to display!
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
@endsection