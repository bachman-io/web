<!doctype html>
<html lang="en" prefix="
og: http://ogp.me/ns#">
<head>

    <title>{{ isset($title) ? $title . ' - Bachman I/O'  : 'Bachman I/O' }}</title>
    <meta name="author" content="Collin Bachman">
    <meta name="description" content="{{ isset($description) ? $description : 'No description available' }}">

    @if( isset($post) && Route::currentRouteName('blog.post'))
        <meta property="og:title" content="{{ $post->title }}" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="{{ url()->current() }}" />
        @if (!is_null($post->title_bg_image_url))
        <meta property="og:image" content="{{ $post->title_bg_image_url }}" />
        @endif
        <meta property="og:description" content="{{ $post->lead_text }}" />
        <meta property="og:site_name" content="Bachman I/O" />
        <meta name="twitter:card" content="summary_large_image" />
    @endif

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel=apple-touch-icon" sizes="180x180" href="{{ env('CDN_URL') }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ env('CDN_URL') }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ env('CDN_URL') }}/favicon-16x16.png">
    <link rel="manifest" href="{{ env('CDN_URL') }}/site.webmanifest">
    <link rel="mask-icon" href="{{ env('CDN_URL') }}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#135ba0">


    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap core CSS -->
    <link href="{{ env('CDN_URL') }}/css/app.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">


</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background: #666;">
    <div class="container">
        <div class="links mx-auto">
            <a href="{{ route('home') }}">
                <img src="{{ env('CDN_URL') }}/bachman-io-logo.svg" width="270" height="30" alt="Bachman I/O">
            </a>
        </div>
        {{--
    <div class="collapse navbar-collapse" id="mainNav">
        <ul class="navbar-nav mr-auto">

            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('blog.index') }}">Blog</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('stories.index') }}">Stories</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('contact') }}">Contact</a>
            </li>
        </ul>

    </div>
    --}}
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="card mb-4" id="twitchCard" style="display: none;">
            <h5 class="card-header text-center">We're Live on Twitch!</h5>
            <div class="card-body">
                <h1 id="twitchTitle"></h1>
                <a href="#" target="_blank" class="btn btn-primary btn-lg">Go to Twitch &rarr;</a>
            </div>
        </div>
    </div>
</div>