
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Some guy on the internet publishes some stores.">
    <meta name="author" content="Collin Bachman">
    <link rel="icon" href="favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title or 'Bachman I/O - Creative Writing by Collin Bachman' }}</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/app.css" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="background: #135ba0;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img src="/images/bachman-io-logo.svg" width="270" height="30" alt="Bachman I/O">
    </a>
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Blog</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Stories</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Contact</a>
            </li>
        </ul>
    </div>
</nav>

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4 text-center">Let's find you something to read.</h1>
        <p class="lead text-center">Personally I recommend either the hipster blog or the overdone stories.</p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="card-group">
            <div class="card mb-4">
                <h5 class="card-header text-center">Latest Story Update</h5>
                <img class="card-img-top" src="http://placehold.it/375x150" alt="Card image cap">
                <div class="card-body">
                    <h1 class="card-title">Story Title</h1>
                    <ul class="list-unstyled">
                        <li><strong>Latest Chapter:</strong> Chapter 1: The Hope Starts with You</li>
                        <li><strong>Status:</strong> Incomplete</li>
                        <li><strong>Age Rating:</strong> Everyone</li>
                        <li><strong>Content Label:</strong> Violence, Gore, Sex, Suggestive, Language</li>
                        <li><strong>Genre:</strong> Adventure</li>
                        <li><strong>Word Count:</strong> 27,257</li>
                    </ul>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary">Story Home &rarr;</a>
                    <a href="#" class="btn btn-secondary">Straight to Chapter 1 &rarr;</a><hr>
                    Posted on January 1, 2017 <br>
                    Last Updated on September 27, 2017<br>
                    by <a href="#">Start Bootstrap</a>
                </div>
            </div>
            <div class="card mb-4">
                <h5 class="card-header text-center">Latest Blog Post</h5>
                <img class="card-img-top" src="http://placehold.it/375x150" alt="Card image cap">
                <div class="card-body">
                    <h1 class="card-title">Post Title</h1>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary">Read More &rarr;</a>
                    <hr>
                    Posted on January 1, 2017 <br>
                    Last Updated on September 27, 2017<br>
                    by <a href="#">Start Bootstrap</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="container">
    <div class="row">

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">Web Design</a>
                                </li>
                                <li>
                                    <a href="#">HTML</a>
                                </li>
                                <li>
                                    <a href="#">Freebies</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                <li>
                                    <a href="#">JavaScript</a>
                                </li>
                                <li>
                                    <a href="#">CSS</a>
                                </li>
                                <li>
                                    <a href="#">Tutorials</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Archive</h5>
                <div class="card-body">
                    <h3>2018</h3>
                    <ul class="list-unstyled mb-0">
                        <li>
                            <a href="#">March</a>
                        </li>
                        <li>
                            <a href="#">February</a>
                        </li>
                        <li>
                            <a href="#">January</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4">Page Heading</h1>

            <!-- Blog Post -->
            <div class="card mb-4">
                <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
                <div class="card-body">
                    <h2 class="card-title">Post Title</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary">Read More &rarr;</a>
                    <hr>
                    Posted on January 1, 2017 <br>
                    Last Updated on September 27, 2017<br>
                    by <a href="#">Start Bootstrap</a>
                </div>
            </div>

            <!-- Blog Post -->
            <div class="card mb-4">
                <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
                <div class="card-body">
                    <h2 class="card-title">Post Title</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary">Read More &rarr;</a>
                    <hr>
                    Posted on January 1, 2017 <br>
                    Last Updated on September 27, 2017<br>
                    by <a href="#">Start Bootstrap</a>
                </div>
            </div>

            <!-- Blog Post -->
            <div class="card mb-4">
                <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
                <div class="card-body">
                    <h2 class="card-title">Post Title</h2>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-primary">Read More &rarr;</a>
                    <hr>
                    Posted on January 1, 2017 <br>
                    Last Updated on September 27, 2017<br>
                    by <a href="#">Start Bootstrap</a>
                </div>
            </div>

            <!-- Pagination -->
            <ul class="pagination justify-content-center mb-4">
                <li class="page-item">
                    <a class="page-link" href="#">&larr; Older</a>
                </li>
                <li class="page-item disabled">
                    <a class="page-link" href="#">Newer &rarr;</a>
                </li>
            </ul>

        </div>

    </div>
    <!-- /.row -->
    </div>

<div class="container">
    <div class="card mb-4">
    <div class="jumbotron jumbotron-fluid" style="background-image: url('http://lorempixel.com/1100/440/'); color: #fff;">
        <div class="container">
            <h1 class="display-4 text-center">Post Title</h1>
            <p class="lead text-center">A short description of the post contents.</p>
        </div>
    </div>
    <div class="card-body">
    <!-- Post Content -->
    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus.</p>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>

    <blockquote class="blockquote">
        <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
        <footer class="blockquote-footer">Someone famous in
            <cite title="Source Title">Source Title</cite>
        </footer>
    </blockquote>

        <pre><code>&lt;p&gt;Sample text here...&lt;/p&gt;
&lt;p&gt;And another line of sample text here...&lt;/p&gt;
</code></pre>

        <p>To edit settings, press <kbd><kbd>ctrl</kbd> + <kbd>,</kbd></kbd></p>

        <p><samp>This text is meant to be treated as sample output from a computer program.</samp></p>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>
    </div>
        <div class="card-footer">
            Posted on January 1, 2017 <br>
            Last Updated on September 27, 2017<br>
            by <a href="#">Start Bootstrap</a>
        </div>
    </div>
</div>
    <!-- /.container -->

<div class="container">
    <div class="card mb-4">
        <div class="jumbotron jumbotron-fluid" style="background-image: url('http://lorempixel.com/1100/440/'); color: #fff;">
            <div class="container">
                <h1 class="display-4 text-center">Long Story Home</h1>
                <p class="lead text-center">Will the description get the best of you?</p>
            </div>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="#"><i class="fas fa-home"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">3</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">4</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">5</a>
                </li>
            </ul>
            <!-- Post Content -->

            <h1>The Nitty-gritty</h1>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">Status</th>
                    <th scope="col">Age Rating</th>
                    <th scope="col">Content Label</th>
                    <th scope="col">Genre</th>
                    <th scope="col">Word Count</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Incomplete</td>
                    <td>Everyone</td>
                    <td>Violence, Gore, Sex, Suggestive, Language</td>
                    <td>Adventure</td>
                    <td>27,257</td>
                </tr>
                </tbody>
            </table>

            <h1>Synopsis</h1>

            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>

            <h1>Author's Notes</h1>
            <dl class="row">
                <dt class="col-sm-3">January 1st, 2017</dt>
                <dd class="col-sm-9">Happy New Year! It's a new story!</dd>

                <dt class="col-sm-3">April 27, 2017</dt>
                <dd class="col-sm-9">
                    <p>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</p>
                    <p>Donec id elit non mi porta gravida at eget metus.</p>
                </dd>
            </dl>
        </div>
        <div class="card-footer">
            Posted on January 1, 2017 <br>
            Last Updated on September 27, 2017<br>
            by <a href="#">Start Bootstrap</a>
        </div>
    </div>
</div>
    <!-- /.container -->

<div class="container">
    <div class="card mb-4">
        <div class="jumbotron jumbotron-fluid" style="background-image: url('http://lorempixel.com/1100/440/'); color: #fff;">
            <div class="container">
                <h1 class="display-4 text-center">Long Story Chapter</h1>
                <p class="lead text-center">Will the description get the best of you?</p>
            </div>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fas fa-home"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="#">1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">3</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">4</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">5</a>
                </li>
            </ul>
            <!-- Post Content -->
            <h1 class="text-center">Chapter 1: The Hope Starts with You</h1>
            <hr />
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus.</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>

            <blockquote class="blockquote">
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                <footer class="blockquote-footer">Someone famous in
                    <cite title="Source Title">Source Title</cite>
                </footer>
            </blockquote>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>

            <hr />
            <!-- Pagination -->
            <ul class="pagination justify-content-center mb-4">
                <li class="page-item">
                    <a class="page-link" href="#">&larr; Previous</a>
                </li>
                <li class="page-item disabled">
                    <a class="page-link" href="#">Next &rarr;</a>
                </li>
            </ul>
        </div>
        <div class="card-footer">
            Posted on January 1, 2017 <br>
            Last Updated on September 27, 2017<br>
            by <a href="#">Start Bootstrap</a>
        </div>
    </div>
    <!-- /.container -->
    <hr>
    <!-- Footer -->
    <footer class="py-5">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; {{ $copyrightYear }} Bachman I/O</p>
        </div>
        <!-- /.container -->
    </footer>
</div>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="js/app.js"></script>
</body>
</html>
