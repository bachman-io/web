@include('layout.head')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav class="nav">
                <a class="nav-link" href="{{ route('post.index') }}">Posts</a>
                <a class="nav-link" href="{{ route('category.index') }}">Categories</a>
                <a class="nav-link" href="{{ route('story.index') }}">Stories</a>
                <a class="nav-link" href="{{ route('genre.index') }}">Genres</a>
                <a class="nav-link" href="{{ route('label.index') }}">Content Labels</a>
                <a class="nav-link disabled" href="#">Users</a>
                <a class="nav-link" href="{{ route('admin.logout') }}">Log Out</a>
            </nav>
        </div>
    </div>
        <h1 class="text-center">{{ $title or 'PAGE TITLE NEEDED' }}</h1>
        @yield('content')
</div>
@include('layout.footer')