<hr>
<footer class="py-5">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; {{ $copyrightYear }} Bachman I/O</p>
    </div>
</footer>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<script defer src="{{ env('CDN_URL') }}/js/app.js"></script>
<script src="https://embed.twitch.tv/embed/v1.js"></script>
<script defer type="text/javascript">
    $(document).ready(function() {
        response = null;
        $.ajax(
            {
                url: '{{ route('twitch') }}',
                data: response,
                dataType: 'json'
            }
        ).done(function(response) {
            if(response.data.length !== 0) {
                $('#twitchTitle').text(response.data[0].title);
                $('#twitchCard').show();
            }
        });
    });
</script>
</body>
</html>