@extends('layout.admin')
@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            <form action="{{ route('post.store') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col">
                        <label for="title">Title</label>
                        <input name="title" id="title" type="text" class="form-control" placeholder="e.g. Fruits" />
                    </div>
                    <div class="col">
                        <label for="slug">Post URL Slug</label>
                        <input name="slug" id="slug" type="text" class="form-control" placeholder="e.g. fruits" />
                    </div>
                    <div class="col">
                        <label for="type">Post Type</label>
                        <select name="type" id="type" class="form-control">
                            <option selected value="Short">Short</option>
                            <option value="Long">Long</option>
                            <option value="Embed">Embed</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="author_name">Author</label>
                        <input type="text" readonly class="form-control-plaintext" id="author_name" value="{{ Auth::user()->name }}" />
                        <input type="hidden" name="author_id" value="{{ Auth::user()->id }}" />
                    </div>
                    <div class="col">
                        <label for="category_id">Category</label>
                        <select name="category_id" id="category_id" class="form-control">
                            @forelse($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @empty
                                <option selected>--No Categories Available--</option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="title_bg_image_url">Title BG Image URL</label>
                        <input name="title_bg_image_url" id="title_bg_image_url" type="text" class="form-control" placeholder="http://www.example.com/image.png" />
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="lead_text">Lead Text</label>
                        <textarea name="lead_text" id="lead_text" class="form-control" rows="5"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="main_text">Main Text</label>
                        <textarea name="main_text" id="main_text" class="form-control" rows="25"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-check">
                            <input name="published" class="form-check-input" type="checkbox" value="1" id="publishCheck">
                            <label class="form-check-label" for="publishCheck">
                                Publish this post?
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-check">
                            <input name="nsfw" class="form-check-input" type="checkbox" value="1" id="nsfwCheck">
                            <label class="form-check-label" for="nsfwCheck">
                                Not Safe for Work?
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
    </div>
@endsection