@extends('layout.admin')
@section('content')
    <a href="{{ route('post.create') }}" class="btn btn-primary">New Post</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Title</th>
            <th scope="col">Type</th>
            <th scope="col">Published?</th>
            <th scope="col">NSFW?</th>
            <th scope="col">Created</th>
            <th scope="col">Updated</th>
            <th scope="col">Author</th>
            <th scope="col">Category</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($posts as $post)
            <tr>
                <td>{{ $post->id }}</td>
                <td>{{ $post->title }}</td>
                <td>{{ $post->type }}</td>
                <td>
                    @if($post->published === 1)
                        Y
                    @else
                        N
                    @endif
                </td>
                <td>
                    @if($post->nsfw === 1)
                        Y
                    @else
                        N
                    @endif
                </td>
                <td>{{ $post->created_at->diffForHumans() }}</td>
                <td>{{ $post->updated_at->diffForHumans() }}</td>
                <td>{{ $post->author->name }}</td>
                <td>{{ $post->category->name }}</td>
                <td><a href="{{ route('post.show', $post->id) }}" target="_blank">Preview</a> <a href="{{ route('post.edit', $post->id) }}">Edit</a></td>
            </tr>
        @empty
            <tr>
                <td colspan="6">There are no posts to display!</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $posts->links() }} <br>
@endsection