@extends('layout.admin')
@section('content')
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Slug</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
    @forelse($categories as $category)
            <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->name }}</td>
                <td>{{ $category->slug }}</td>
                <td><a href="{{ route('category.edit', $category->id) }}">Edit</a></td>
            </tr>
    @empty
        <tr>
            <td colspan="4">There are no categories to display!</td>
        </tr>
    @endforelse
        </tbody>
    </table>
    <a href="{{ route('category.create') }}" class="btn btn-primary">New Category</a>
@endsection