@extends('layout.admin')
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('label.update', $label->id) }}" method="post">
        <input name="_method" type="hidden" value="PUT">
        @csrf
        <div class="row">
            <div class="col">
                <label for="name">Content Label Name</label>
                <input name="name" id="name" type="text" class="form-control" value="{{ $label->name }}" />
            </div>
            <div class="col">
                <label for="slug">Content Label URL Slug</label>
                <input name="slug" id="slug" type="text" class="form-control" value="{{ $label->slug }}" />
            </div>
        </div>
        <div class="row">
            <div class="col">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection