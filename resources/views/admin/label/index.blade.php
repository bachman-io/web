@extends('layout.admin')
@section('content')
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Slug</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($labels as $label)
            <tr>
                <td>{{ $label->id }}</td>
                <td>{{ $label->name }}</td>
                <td>{{ $label->slug }}</td>
                <td><a href="{{ route('label.edit', $label->id) }}">Edit</a></td>
            </tr>
        @empty
            <tr>
                <td colspan="4">There are no content labels to display!</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <a href="{{ route('label.create') }}" class="btn btn-primary">New Content Label</a>
@endsection