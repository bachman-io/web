@extends('layout.admin')
@section('content')
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Slug</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($genres as $genre)
            <tr>
                <td>{{ $genre->id }}</td>
                <td>{{ $genre->name }}</td>
                <td>{{ $genre->slug }}</td>
                <td><a href="{{ route('genre.edit', $genre->id) }}">Edit</a></td>
            </tr>
        @empty
            <tr>
                <td colspan="4">There are no genres to display!</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    <a href="{{ route('genre.create') }}" class="btn btn-primary">New Genre</a>
@endsection