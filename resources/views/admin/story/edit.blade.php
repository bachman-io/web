@extends('layout.admin')
@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('story.update', ['story' => $story->id]) }}" method="post">
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="row">
                <div class="col">
                    <label for="title">Title</label>
                    <input name="title" id="title" type="text" class="form-control" value="{{ $story->title }}" />
                </div>
                <div class="col">
                    <label for="slug">Story URL Slug</label>
                    <input name="slug" id="slug" type="text" class="form-control" value="{{ $story->slug }}" />
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="author_name">Author</label>
                    <input type="text" readonly class="form-control-plaintext" id="author_name" value="{{ $story->author->name }}" />
                    <input type="hidden" name="author_id" value="{{ $story->author->id }}" />
                </div>
                <div class="col">
                    <label for="genre_id">Genre</label>
                    <select name="genre_id" id="genre_id" class="form-control">
                        @forelse($genres as $genre)
                            <option {{ $genre->id === $story->genre->id ? 'selected' : '' }} value="{{ $genre->id }}">{{ $genre->name }}</option>
                        @empty
                            <option selected>--No Genres Available--</option>
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="title_color">Title Color</label><br>
                    <input name="title_color" type="color" id="title_color" class=form-control" value="{{ $story->title_color }}" />
                </div>
                <div class="col">
                    <label for="title_bg_color">Title BG Color</label><br>
                    <input name="title_bg_color" type="color" id="title_bg_color" class=form-control" value="{{ $story->title_bg_color }}" />
                </div>
                <div class="col">
                    <label for="title_bg_image_url">Title BG Image URL</label>
                    <input name="title_bg_image_url" id="title_bg_image_url" type="text" class="form-control" value="{{ $story->title_bg_image_url }}" />
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option {{ $story->status === 'In Progress' ? 'selected' : '' }} value="In Progress">In Progress</option>
                        <option {{ $story->status === 'Complete' ? 'selected' : '' }} value="Complete">Complete</option>
                        <option {{ $story->status === 'Cancelled' ? 'selected' : '' }} value="Cancelled">Cancelled</option>
                    </select>
                </div>
                <div class="col">
                    <label for="age_rating">Age Rating</label>
                    <select name="age_rating" id="age_rating" class="form-control">
                        <option {{ $story->age_rating === 'Everyone' ? 'selected' : '' }} value="Everyone">Everyone</option>
                        <option {{ $story->age_rating === 'Teen' ? 'selected' : '' }} value="Teen">Teen</option>
                        <option {{ $story->age_rating === 'Adult' ? 'selected' : '' }} value="Adult">Adult</option>
                    </select>
                </div>
                <div class="col">
                    <label for="content_labels">Content Labels</label>
                    <select multiple name="content_labels[]" id="content_labels" class="form-control">
                        @forelse($labels as $label)
                            <option {{ array_key_exists($label->id, $contentLabelIds) ? 'selected' : '' }} value="{{ $label->id }}">{{ $label->name }}</option>
                        @empty
                            <option selected>--No Content Labels Available--</option>
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="synopsis">Synopsis</label>
                    <textarea name="synopsis" id="synopsis" class="form-control" rows="5">{{ $story->synopsis }}</textarea>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-check">
                        <input {{ $story->published ? 'checked' : '' }} name="published" class="form-check-input" type="checkbox" value="1" id="publishCheck">
                        <label class="form-check-label" for="publishCheck">
                            Publish this post?
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection