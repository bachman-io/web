@extends('layout.admin')
@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('note.update', ['story' => $story->id, 'note' => $note->id]) }}" method="post">
            @csrf
            <input name="_method" type="hidden" value="PUT">
            <div class="row">
                <div class="col">
                    <label for="text">Note Text</label>
                    <textarea name="text" id="text" class="form-control" rows="10">{{ $note->text }}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection