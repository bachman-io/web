@extends('layout.admin')
@section('content')
    <a href="{{ route('note.create', ['story' => $story->id]) }}" class="btn btn-primary">New Note</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">Created</th>
            <th scope="col">Text</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($story->notes as $note)
            <tr>
                <td>{{ $noteDates[$note->id] }}</td>
                <td>{{ $note->text }}</td>
                <td><a href="{{ route('note.edit', ['story' => $story->id, 'note' => $note->id]) }}">Edit</a></td>
            </tr>
        @empty
            <tr>
                <td colspan="6">There are no notes to display!</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection