@extends('layout.admin')
@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('story.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col">
                    <label for="title">Title</label>
                    <input name="title" id="title" type="text" class="form-control" placeholder="e.g. Fruits" />
                </div>
                <div class="col">
                    <label for="slug">Story URL Slug</label>
                    <input name="slug" id="slug" type="text" class="form-control" placeholder="e.g. fruits" />
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="author_name">Author</label>
                    <input type="text" readonly class="form-control-plaintext" id="author_name" value="{{ Auth::user()->name }}" />
                    <input type="hidden" name="author_id" value="{{ Auth::user()->id }}" />
                </div>
                <div class="col">
                    <label for="genre_id">Genre</label>
                    <select name="genre_id" id="genre_id" class="form-control">
                        @forelse($genres as $genre)
                            <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                        @empty
                            <option selected>--No Genres Available--</option>
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="title_color">Title Color</label><br>
                    <input name="title_color" type="color" id="title_color" class=form-control" value="#000000" />
                </div>
                <div class="col">
                    <label for="title_bg_color">Title BG Color</label><br>
                    <input name="title_bg_color" type="color" id="title_bg_color" class=form-control" value="#ffffff" />
                </div>
                <div class="col">
                    <label for="title_bg_image_url">Title BG Image URL</label>
                    <input name="title_bg_image_url" id="title_bg_image_url" type="text" class="form-control" placeholder="http://www.example.com/image.png" />
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="form-control">
                        <option selected value="In Progress">In Progress</option>
                        <option value="Complete">Complete</option>
                        <option value="Cancelled">Cancelled</option>
                    </select>
                </div>
                <div class="col">
                    <label for="age_rating">Age Rating</label>
                    <select name="age_rating" id="age_rating" class="form-control">
                        <option selected value="Everyone">Everyone</option>
                        <option value="Teen">Teen</option>
                        <option value="Adult">Adult</option>
                    </select>
                </div>
                <div class="col">
                    <label for="content_labels">Content Labels</label>
                    <select multiple name="content_labels[]" id="content_labels" class="form-control">
                        @forelse($labels as $label)
                            <option value="{{ $label->id }}">{{ $label->name }}</option>
                        @empty
                            <option selected>--No Content Labels Available--</option>
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="synopsis">Synopsis</label>
                    <textarea name="synopsis" id="synopsis" class="form-control" rows="5"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-check">
                        <input name="published" class="form-check-input" type="checkbox" value="1" id="publishCheck">
                        <label class="form-check-label" for="publishCheck">
                            Publish this post?
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection