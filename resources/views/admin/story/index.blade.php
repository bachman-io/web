@extends('layout.admin')
@section('content')
    <a href="{{ route('story.create') }}" class="btn btn-primary">New Story</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Title</th>
            <th scope="col">Published?</th>
            <th scope="col">Created</th>
            <th scope="col">Updated</th>
            <th scope="col">Author</th>
            <th scope="col">Status</th>
            <th scope="col">Genre</th>
            <th scope="col">Age</th>
            <th scope="col">Labels</th>
            <th scope="col">Word Count</th>
        </tr>
        </thead>
        <tbody>
        @forelse($stories as $story)
            <tr>
                <td rowspan="2">{{ $story->id }}</td>
                <td>{{ $story->title }}</td>
                <td>
                    @if($story->published === 1)
                        Y
                    @else
                        N
                    @endif
                </td>
                <td>{{ $story->created_at->diffForHumans() }}</td>
                <td>{{ $story->updated_at->diffForHumans() }}</td>
                <td>{{ $story->author->name }}</td>
                <td>{{ $story->status }}</td>
                <td>{{ $story->genre->name }}</td>
                <td>{{ $story->age_rating }}</td>
                <td>{{ $labelString[$story->id] }}</td>
                <td>{{ number_format($wordCount[$story->id]) }}</td>
            </tr>
            <tr>
                <td colspan="9">
                    <a href="{{ route('story.edit', ['story' => $story->id]) }}" class="btn btn-primary">Story Metadata</a> <a href="{{ route('chapter.index', ['story' => $story->id]) }}" class="btn btn-primary">Chapters</a> <a href="{{ route('note.index', ['story' => $story->id]) }}" class="btn btn-primary">Author Notes</a>
                </td>
            </tr>
        @empty            <tr>
                <td colspan="6">There are no stories to display!</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $stories->links() }}
@endsection