@extends('layout.admin')
@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('chapter.update', ['story' => $story->id, 'chapter' => $chapter->story_chapter_number]) }}" method="post">
            @csrf
            <input name="_method" type="hidden" value="PUT">
            <div class="row">
                <div class="col">
                    <label for="story_chapter_number">Chapter #</label>
                    <input readonly="readonly" name="story_chapter_number" id="story_chapter_number" type="number" class="form-control-plaintext" value="{{ $chapter->story_chapter_number }}" />
                </div>
                <div class="col">
                    <label for="title">Title</label>
                    <input name="title" id="title" type="text" class="form-control" value="{{ $chapter->title }}" />
                </div>
                <div class="col">
                    <label for="slug">Chapter URL Slug</label>
                    <input name="slug" id="slug" type="text" class="form-control" value="{{ $chapter->slug }}" />
                </div>
                <div class="col">
                    <label for="word_count">Word Count</label>
                    <input name="word_count" id="word_count" type="number" class="form-control" value="{{ $chapter->word_count }}" />
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="text">Chapter Text</label>
                    <textarea name="text" id="text" class="form-control" rows="25">{{ $chapter->text }}</textarea>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-check">
                        <input name="published" class="form-check-input" type="checkbox" value="1" id="publishCheck" @if($chapter->published === 1) checked @endif>
                        <label class="form-check-label" for="publishCheck">
                            Publish this post?
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection