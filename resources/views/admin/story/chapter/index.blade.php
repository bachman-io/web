@extends('layout.admin')
@section('content')
    <a href="{{ route('chapter.create', ['story' => $story->id]) }}" class="btn btn-primary">New Chapter</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Published?</th>
            <th scope="col">Created</th>
            <th scope="col">Updated</th>
            <th scope="col">Word Count</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($story->chapters as $chapter)
            <tr>
                <td rowspan="2">{{ $chapter->story_chapter_number }}</td>
                <td>{{ $chapter->title }}</td>
                <td>
                    @if($chapter->published === 1)
                        Y
                    @else
                        N
                    @endif
                </td>
                <td>{{ $chapter->created_at->diffForHumans() }}</td>
                <td>{{ $chapter->updated_at->diffForHumans() }}</td>
                <td>{{ number_format($chapter->word_count) }}</td>
                <td><a href="{{ route('chapter.edit', ['story' => $story->id, 'chapter' => $chapter->story_chapter_number]) }}">Edit</a></td>
            </tr>
        @empty
            <tr>
            <td colspan="6">There are no chapters to display!</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection