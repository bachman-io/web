<!-- Sidebar Widgets Column -->
<div class="col-md-4 d-none d-md-block">
    <h1>BachBlog</h1>
    <!-- Categories Widget -->
    <div class="card">
        <h5 class="card-header">Stalk Me</h5>
        <div class="card-body">
            <ul class="list-unstyled">
                <li><a href="https://anilist.co/user/BachMac" target="_blank">AniList</a></li>
                <li><a href="https://github.com/bachman-io" target="_blank">GitHub</a></li>
                <li><a href="https://open.spotify.com/user/n3pneawifryp4l7z1il67ma6o?si=7B31mzaFRS23qD06vQVABg" target="_blank">Spotify</a></li>
            </ul>
        </div>
    </div>

    <div class="card my-4">
        <h5 class="card-header">Categories</h5>
        <div class="card-body">
            <ul class="list-unstyled mb-0">
                @forelse($categories as $category)
                    <li><a href="{{ route('blog.category', ['id' => $category->id, 'slug' => $category->slug]) }}">{{ $category->name }} <span class="badge badge-secondary">{{ $category->posts_count }}</span></a></li>
                @empty
                    <li>There are no categories to display!</li>
                @endforelse
            </ul>
        </div>
    </div>

    <!-- Side Widget -->
    <div class="card my-4">
        <h5 class="card-header">Archive</h5>
        <div class="card-body">
            @forelse($archive_months as $year => $month)
                <h3>{{ $year }}</h3>
                <ul class="list-unstyled mb-0">
                    @foreach($month as $m)
                        <li>
                            <a href="{{ route('blog.archive', ['year' => $year, 'month' => $m->month]) }}">{{ $m->month_name }} <span class="badge badge-secondary">{{ $m->posts_count }}</span></a>
                        </li>
                    @endforeach
                </ul>
            @empty
                There are no archives to display!
            @endforelse
        </div>
    </div>

</div>