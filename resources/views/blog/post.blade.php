@extends('layout.default')
@section('content')
    <div class="container">
        @if ($post->type === 'Long')
        <div class="card mb-4">
            <h5 class="card-header" @if ($post->nsfw === 1) style="background: #a02313;" @endif><a href="{{ route('home') }}">← Back to Latest Posts!</a></h5>
            @if(!is_null($post->title_bg_image_url))
                <img class="card-img-top" src="{{ $post->title_bg_image_url }}" alt="{{ $post->title }}">
            @else
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        @if ($post->nsfw === 1)
                        <h1 class="display-4 text-center">{{ '[NSFW] ' . $post->title }}</h1>
                        @else
                        <h1 class="display-4 text-center">{{ $post->title }}</h1>
                        @endif
                    </div>
                </div>
            @endif
            <div class="card-body">
                <p class="lead text-center">{{ $post->lead_text }}</p>
                <hr>
                {!! $markdown['main'] !!}
            </div>
            <div class="card-footer">
                Posted {{ $postDates['created'] }}<br>
                @if($post->created_at->format('ymd') < $post->updated_at->format('ymd'))
                Last Updated {{ $postDates['updated'] }}<br>
                @endif
                by {{ $post->author->name }}<br>
                in <a href="{{ route('blog.category', ['id' => $post->category->id, 'slug' => $post->category->slug]) }}">{{ $post->category->name }}</a>
            </div>
        </div>
            @else
            <div class="row">
                @include('blog.sidebar')
                <div class="col-md-8">
                    <div class="card mb-4">
                    <h5 class="card-header" @if ($post->nsfw === 1) style="background: #a02313;" @endif><a href="{{ route('home') }}">← Back to Latest Posts!</a></h5>
                        <div class="card-body">
                        @switch($post->type)
                            @case('Short')
                            <h2 class="card-title">{{ $post->lead_text }}</h2>
                            @break
                            @case('Embed')
                            <h2 class="card-title">{{ $post->title }}</h2>
                            {!! $post->lead_text !!}
                        @endswitch
                        </div>
                        <div class="card-footer">
                            Posted {{ $postDates['created'] }}<br>
                            @if($post->created_at->format('ymd') < $post->updated_at->format('ymd'))
                                Last Updated {{ $postDates['updated'] }}<br>
                            @endif
                            by {{ $post->author->name }}<br>
                            in <a href="{{ route('blog.category', ['id' => $post->category->id, 'slug' => $post->category->slug]) }}">{{ $post->category->name }}</a>
                        </div>
                    </div>
                </div>
            </div>
            @endif
    </div>
@endsection