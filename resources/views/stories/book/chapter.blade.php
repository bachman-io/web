@extends('layout.default')
@section('content')
    <div class="container">
        <div class="card mb-4">
            <h5 class="card-header"><a href="{{ route('stories.index') }}">← Back to The Stories!</a></h5>
            @if(!is_null($story->title_bg_image_url))
                <img class="card-img-top" src="{{ $story->title_bg_image_url }}" alt="{{ $story->title }}">
            @else
                <div class="jumbotron jumbotron-fluid" style="background: {{ $story->title_bg_color }}">
                    <div class="container">
                        <h1 class="display-4 text-center">{{ $story->title }}</h1>
                    </div>
                </div>
            @endif
            <div class="card-body">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('stories.book', ['story_id' => $story->id, 'chapter_id' => 0, 'story_slug' => $story->slug, 'chapter_slug' => 'home']) }}"><i class="fas fa-home"></i></a>
                    </li>
                    @if(!is_null($story->chapters))
                        @foreach($story->chapters as $chapter)
                            <li class="nav-item">
                                @if($chapter->published == 1)
                                    <a class="nav-link {{ $chapter->story_chapter_number === $currentChapter->story_chapter_number ? "active" : "" }}" href="{{ route('stories.book', ['story_id' => $story->id, 'chapter_id' => $chapter->story_chapter_number, 'story_slug' => $story->slug, 'chapter_slug' => $chapter->slug]) }}">{{ $chapter->story_chapter_number }}</a>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
                <!-- Post Content -->

                <h1 class="text-center">{{ $currentChapter->title }}</h1>
                <hr>
                {!! $markdown['main'] !!}
            </div>
            <div class="card-footer">
                Posted {{ $chapterDates['created']}}<br>
                @if($currentChapter->created_at->format('ymd') < $story->updated_at->format('ymd'))
                    Last Updated {{ $chapterDates['updated'] }}<br>
                @endif
                by {{ $story->author->name }}
            </div>
        </div>
    </div>
@endsection