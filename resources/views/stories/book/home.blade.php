@extends('layout.default')
@section('content')
    <div class="container">
        <div class="card mb-4">
            <h5 class="card-header"><a href="{{ route('stories.index') }}">← Back to The Stories!</a></h5>
            @if(!is_null($story->title_bg_image_url))
                <img class="card-img-top" src="{{ $story->title_bg_image_url }}" alt="{{ $story->title }}">
            @else
                <div class="jumbotron jumbotron-fluid" style="background: {{ $story->title_bg_color }}">
                    <div class="container">
                        <h1 class="display-4 text-center">{{ $story->title }}</h1>
                    </div>
                </div>
            @endif
            <div class="card-body">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('stories.book', ['story_id' => $story->id, 'chapter_id' => 0, 'story_slug' => $story->slug, 'chapter_slug' => 'home']) }}"><i class="fas fa-home"></i></a>
                    </li>
                    @if(!is_null($story->chapters))
                    @foreach($story->chapters as $chapter)
                    <li class="nav-item">
                        @if($chapter->published == 1)
                        <a class="nav-link" href="{{ route('stories.book', ['story_id' => $story->id, 'chapter_id' => $chapter->story_chapter_number, 'story_slug' => $story->slug, 'chapter_slug' => $chapter->slug]) }}">{{ $chapter->story_chapter_number }}</a>
                        @endif
                    </li>
                    @endforeach
                    @endif
                </ul>
                <!-- Post Content -->

                <h1>The Nitty-gritty</h1>
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">Status</th>
                        <th scope="col">Age Rating</th>
                        <th scope="col">Content Label</th>
                        <th scope="col">Genre</th>
                        <th scope="col">Word Count</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ $story->status }}</td>
                        <td>{{ $story->age_rating }}</td>
                        <td>{{ $labelString }}</td>
                        <td>{{ $story->genre->name }}</td>
                        <td>{{ number_format($wordCount) }}</td>
                    </tr>
                    </tbody>
                </table>

                <h1>Synopsis</h1>

                <p class="lead">{{ $story->synopsis }}</p>

                <h1>Author's Notes</h1>
                @if($story->notes_count > 0)
                <dl class="row">
                    @foreach($story->notes as $note)
                    <dt class="col-sm-3">{{ $noteDates[$note->id] }}</dt>
                    <dd class="col-sm-9"><p>{{ $note->text }}</p></dd>
                    @endforeach
                </dl>
                @else
                <p>There are no Author's Notes for this story!</p>
                @endif

            </div>
            <div class="card-footer">
                Posted {{ $storyDates['created']}}<br>
                @if($story->created_at->format('ymd') < $story->updated_at->format('ymd'))
                    Last Updated {{ $storyDates['updated'] }}<br>
                @endif
                by {{ $story->author->name }}
            </div>
        </div>
    </div>
@endsection    