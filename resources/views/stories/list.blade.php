@extends('layout.default')
@section('content')
    <div class="container">
        <div class="row">

            <!-- Sidebar Widgets Column -->
            <div class="col-md-4 d-none d-md-block">

                <!-- genres Widget -->
                <div class="card my-4">
                    <h5 class="card-header">Genres</h5>
                    <div class="card-body">
                        <ul class="list-unstyled mb-0">
                            @forelse($genres as $genre)
                                <li><a href="{{ route('stories.genre', ['id' => $genre->id, 'slug' => $genre->slug]) }}">{{ $genre->name }} <span class="badge badge-secondary">{{ $genre->stories_count }}</span></a></li>
                            @empty
                                <li>There are no genres to display!</li>
                            @endforelse
                        </ul>
                    </div>
                </div>

            </div>

            <!-- Stories Column -->
            <div class="col-md-8">

                <h1 class="my-4">{{ $title }}</h1>

                <!-- Blog Post -->
                @forelse($stories as $story)
                    @if($story->published_chapters > 0)
                    <div class="card mb-4">
                        @if(!is_null($story->title_bg_image_url))
                            <img class="card-img-top" src="{{ $story->title_bg_image_url }}" alt="{{ $story->title }}">
                        @endif
                        <div class="card-body">
                            @if(is_null($story->title_bg_image_url))
                            <h2 class="card-title">{{ $story->title }}</h2>
                            @endif
                            <p class="card-text">{{ $story->synopsis }}</p>
                            <ul class="list-unstyled">
                                <li><strong>Author:</strong> {{ $story->author->name }}</li>
                                <li><strong>Latest Chapter:</strong> {{ $latestChapter[$story->id]->story_chapter_number . ': ' . $latestChapter[$story->id]->title }}</li>
                                <li><strong>Status:</strong> {{ $story->status }}</li>
                                <li><strong>Age Rating:</strong> {{ $story->age_rating }}</li>
                                <li><strong>Content Label:</strong> {{ $labelString[$story->id] }}</li>
                                <li><strong>Genre:</strong> {{ $story->genre->name }}</li>
                                <li><strong>Word Count:</strong> {{ number_format($wordCount[$story->id]) }}</li>
                            </ul>
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('stories.book', ['story_id' => $story->id, 'chapter_id' => 0, 'story_slug' => $story->slug, 'chapter_slug' => 'home']) }}" class="btn btn-primary">Story Home &rarr;</a>
                            <a href="{{ route('stories.book', ['story_id' => $story->id, 'chapter_id' => $latestChapter[$story->id]->story_chapter_number, 'story_slug' => $story->slug, 'chapter_slug' => $latestChapter[$story->id]->slug]) }}" class="btn btn-secondary">Straight to Chapter {{ $latestChapter[$story->id]->story_chapter_number }} &rarr;</a>
                        </div>
                    </div>
                    @endif
                @empty
                    <h2>(Well, it would be if we had any to display...)</h2>
                @endforelse


                {{ $stories->links() }}

            </div>

        </div>
        <div class="row">
            <div class="col-md-12 d-md-none">
                <div class="card my-4">
                    <h5 class="card-header">genres</h5>
                    <div class="card-body">
                        <ul class="list-inline mb-0">
                            @forelse($genres as $g)
                                <li class="list-inline-item"><a href="{{ route('stories.genre', ['id' => $g->id, 'slug' => $g->slug]) }}">{{ $g->name }} <span class="badge badge-secondary">{{ $g->stories_count }}</span></a></li>
                            @empty
                                <li>There are no genres to display!</li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
@endsection