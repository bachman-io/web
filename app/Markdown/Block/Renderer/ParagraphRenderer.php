<?php

namespace App\Markdown\Block\Renderer;

use League\CommonMark\Block\Element\AbstractBlock;
use League\CommonMark\Block\Element\Paragraph;
use League\CommonMark\ElementRendererInterface;
use League\CommonMark\HtmlElement;
use League\CommonMark\Util\Xml;
use League\CommonMark\Block\Renderer\BlockRendererInterface;

class ParagraphRenderer implements BlockRendererInterface
{
    /**
     * @param Paragraph                $block
     * @param ElementRendererInterface $htmlRenderer
     * @param bool                     $inTightList
     *
     * @return HtmlElement|string
     */
    public function render(AbstractBlock $block,
        ElementRendererInterface $htmlRenderer, $inTightList = false)
    {
        if (!($block instanceof Paragraph)) {
            throw new \InvalidArgumentException('Incompatible block type: ' . get_class($block));
        }

        if ($inTightList) {
            return $htmlRenderer->renderInlines($block->children());
        } else {
            $attrs = [];
            foreach ($block->getData('attributes', []) as $key => $value) {
                $attrs[$key] = Xml::escape($value, true);
            }

            $attrs['style'] = 'line-height: 2rem;';

            return new HtmlElement('p', $attrs, $htmlRenderer->renderInlines($block->children()));
        }
    }
}
