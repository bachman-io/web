<?php

namespace App;

use App\Http\Requests\StoreGenre;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{

    /**
     * Fillable fields when mass-assigning the Model
     *
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    /**
     * Returns collection of stories with this genre
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stories()
    {
        return $this->hasMany('App\Story');
    }

    /**
     * Genres in alphabetical order, or null if none
     *
     * @return mixed
     */
    public function alphabetic()
    {
        return $this->orderBy('name')->get();
    }

    /**
     * Genre names and story counts for sidebar,
     * or null if none
     *
     * @return mixed
     */
    public function genreLinks()
    {
        return $this->whereHas('stories', function ($q) {
            $q->where('published', true);
        })->withCount(['stories' => function ($q) {
            $q->where('published', true);
        }])->orderBy('name')->get();
    }

    /**
     * List of genre's stories, or null if none
     *
     * @param $id
     * @return mixed
     */
    public function genreList($id)
    {
        return $this->find($id)->stories()
            ->where('published', 1)
            ->whereHas('chapters')
            ->withCount([
                'chapters as published_chapters' => function ($query) {
                    $query->where('published', true);
                }
            ])
            ->orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Add a new genre
     *
     * @param StoreGenre $request
     */
    public function addNew(StoreGenre $request)
    {
        $this->name = $request->name;
        $this->slug = $request->slug;
        $this->save();
    }

    /**
     * Update an existing genre
     *
     * @param $id
     * @param StoreGenre $request
     */
    public function updateExisting($id, StoreGenre $request)
    {
        $genre = $this->find($id);
        $genre->name = $request->name;
        $genre->slug = $request->slug;
        $genre->save();
    }
}
