<?php

namespace App;

use App\Http\Requests\StoreAuthorNote;
use App\Http\Requests\StoreChapter;
use App\Http\Requests\StoreStory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Story extends Model
{

    public function author()
    {
        return $this->belongsTo('App\User');
    }

    public function labels()
    {
        return $this->belongsToMany('App\Label');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function chapters()
    {
        return $this->hasMany('App\Chapter');
    }

    public function notes()
    {
        return $this->hasMany('App\AuthorNote');
    }

    public function allList()
    {
        return $this->orderBy('created_at', 'desc')
            ->paginate(10);
    }

    public function latestOne()
    {
        return $this->with(['author', 'chapters', 'labels'])
            ->where([
                ['published', '=', 1],
                ['status', '<>', 'Cancelled']
            ])
            ->orderBy('created_at', 'desc')
            ->first();
    }

    public function latestList()
    {
        return $this->where('published', 1)
            ->whereHas('chapters')
            ->withCount([
                'chapters as published_chapters' => function ($query) {
                    $query->where('published', 1);
                }])
            ->orderBy('created_at', 'desc')
            ->paginate(10);
    }

    public function getStory($id)
    {
        return $this->with(['chapters' => function ($query) {
            $query->orderBy('story_chapter_number');
        }])->withCount([
            'notes',
            ])
            ->findOrFail($id);
    }

    public function getChapter($story, $chapter_id)
    {
        return $story->chapters()->where('story_chapter_number', $chapter_id)->firstOrFail();
    }

    public function handleSingleStoryDates($story)
    {
        $storyDates['created'] = $story->created_at->diffForHumans();
        $storyDates['updated'] = $story->updated_at->diffForHumans();

        if ($story->created_at->diffInDays() >= 7) {
            $storyDates['created'] = $story->created_at->format('F jS');
        }
        if (Carbon::now()->format('Y') > $story->created_at->format('Y')) {
            $storyDates['created'] = $story->created_at->format('n/j/Y');
        }

        if ($story->updated_at->diffInDays() >= 7) {
            $storyDates['updated'] = $story->updated_at->format('F jS');
        }
        if (Carbon::now()->format('Y') > $story->updated_at->format('Y')) {
            $storyDates['updated'] = $story->updated_at->format('n/j/Y');
        }


        return $storyDates;
    }

    public function handleChapterDates($chapter)
    {
        $chapterDates['created'] = $chapter->created_at->diffForHumans();
        $chapterDates['updated'] = $chapter->updated_at->diffForHumans();

        if ($chapter->created_at->diffInDays() >= 7) {
            $chapterDates['created'] = $chapter->created_at->format('F jS');
        }
        if (Carbon::now()->format('Y') > $chapter->created_at->format('Y')) {
            $chapterDates['created'] = $chapter->created_at->format('n/j/Y');
        }

        if ($chapter->updated_at->diffInDays() >= 7) {
            $chapterDates['updated'] = $chapter->updated_at->format('F jS');
        }
        if (Carbon::now()->format('Y') > $chapter->updated_at->format('Y')) {
            $chapterDates['updated'] = $chapter->updated_at->format('n/j/Y');
        }


        return $chapterDates;
    }

    public function handleNoteDates($story)
    {
        $noteDates = [];
        if (!is_null($story)) {
            foreach ($story->notes as $note) {
                $noteDates[$note->id] = $note->created_at->diffForHumans();

                if ($note->created_at->diffInDays() >= 7) {
                    $noteDates[$note->id] = $note->created_at->format('F jS');
                }
                if (Carbon::now()->format('Y') > $note->created_at->format('Y')) {
                    $noteDates[$note->id] = $note->created_at->format('n/j/Y');
                }
            }
        }

        return $noteDates;
    }

    public function latestChapter($story)
    {
        return $story->chapters()
            ->orderBy('story_chapter_number', 'desc')
            ->first();
    }

    public function latestChapters($stories)
    {
        $latestChapter = [];

        foreach ($stories as $story) {
            $latestChapter[$story->id] = $story->chapters()
                ->orderBy('story_chapter_number', 'desc')
                ->first();
        }

        return $latestChapter;
    }

    public function labelString($story)
    {
        $firstRun = true;
        $labelString = '';

        foreach ($story->labels as $label) {
            if ($firstRun) {
                $labelString = $label->name;
                $firstRun = false;
            } else {
                $labelString .= ', ' . $label->name;
            }
        }

        return $labelString;
    }

    public function labelStrings($stories)
    {
        $labelString = [];

        foreach ($stories as $story) {
            $firstRun = true;
            $ls = '';

            foreach ($story->labels as $label) {
                if ($firstRun) {
                    $ls = $label->name;
                    $firstRun = false;
                } else {
                    $ls .= ', ' . $label->name;
                }
            }

            $labelString[$story->id] = $ls;
        }

        return $labelString;
    }

    public function wordCount()
    {
        return $this->chapters->sum('word_count');
    }

    public function wordCounts($stories)
    {
        $wordCounts = [];

        foreach ($stories as $story) {
            $wordCounts[$story->id] = $story->chapters()->sum('word_count');
        }

        return $wordCounts;
    }

    public function contentLabelIds($story)
    {
        $contentLabelIds = [];

        foreach ($story->labels as $label) {
            $contentLabelIds[$label->id] = $label->id;
        }

        return $contentLabelIds;
    }

    public function addNew(StoreStory $request)
    {
        $this->title = $request->title;
        $this->slug = $request->slug;
        if (isset($request->published)) {
            $this->published = $request->published;
        }
        $this->title_color = $request->title_color;
        $this->title_bg_color = $request->title_bg_color;
        $this->title_bg_image_url = $request->title_bg_image_url;
        $this->author_id = $request->author_id;
        $this->genre_id = $request->genre_id;
        $this->status = $request->status;
        $this->age_rating = $request->age_rating;
        $this->synopsis = $request->synopsis;
        $this->save();

        $createdStory = $this->find($this->id);

        $createdStory->labels()->sync($request->content_labels);
    }

    public function updateExisting($id, StoreStory $request)
    {

        $story = $this->find($id);
        $story->title = $request->title;
        $story->slug = $request->slug;
        if (isset($request->published)) {
            $story->published = $request->published;
        }
        $story->title_color = $request->title_color;
        $story->title_bg_color = $request->title_bg_color;
        $story->title_bg_image_url = $request->title_bg_image_url;
        $story->author_id = $request->author_id;
        $story->genre_id = $request->genre_id;
        $story->status = $request->status;
        $story->age_rating = $request->age_rating;
        $story->synopsis = $request->synopsis;
        $story->labels()->sync($request->content_labels);
        $story->save();
    }

    public function addChapter($story_id, StoreChapter $request)
    {
        $published = isset($request->published) ? 1 : 0;
        $story = $this->find($story_id);
        $chapter = new Chapter([
            'story_chapter_number' => $request->story_chapter_number,
            'title' => $request->title,
            'slug' => $request->slug,
            'published' => $published,
            'text' => $request->text,
            'word_count' => $request->word_count
        ]);

        $story->chapters()->save($chapter);
    }

    public function addAuthorNote($story_id, StoreAuthorNote $request)
    {
        $story = $this->find($story_id);
        $note = new AuthorNote([
            'text' => $request->text
        ]);

        $story->notes()->save($note);
    }
}
