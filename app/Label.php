<?php

namespace App;

use App\Http\Requests\StoreLabel;
use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    protected $fillable = ['name'];

    public function stories()
    {
        return $this->belongsToMany('App\Story');
    }

    public function alphabetic()
    {
        return $this->orderBy('name')->get();
    }

    public function addNew(StoreLabel $request)
    {
        $this->name = $request->name;
        $this->slug = $request->slug;
        $this->save();
    }

    public function updateExisting($id, StoreLabel $request)
    {
        $label = $this->find($id);
        $label->name = $request->name;
        $label->slug = $request->slug;
        $label->save();
    }
}
