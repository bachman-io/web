<?php

namespace App;

use App\Http\Requests\StoreCategory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['name', 'slug'];


    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function alphabetic()
    {
        return $this->orderBy('name')->get();
    }

    public function categoryLinks($data)
    {
        $data['categories'] = $this->whereHas('posts', function ($q) {
            $q->where('published', true);
        })->withCount(['posts' => function ($q) {
            $q->where('published', true);
        }])->orderBy('name')->get();
        return $data;
    }

    public function addNew(StoreCategory $request)
    {
        $this->name = $request->name;
        $this->slug = $request->slug;
        $this->save();
    }

    public function updateExisting($id, StoreCategory $request)
    {
        $category= $this->find($id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->save();
    }
}
