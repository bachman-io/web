<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

/**
 * Class GlobalViewComposer
 * @package App\Http\ViewComposers
 */
class GlobalViewComposer
{

    /**
     * Array of variables to pass into all views in the app
     *
     * @var GlobalViewComposer $viewArray
     */
    public $viewArray;



    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $this->viewArray['copyrightYear'] = $this->getCopyrightYear();
        $view->with($this->viewArray);
    }

    /**
     * Gives us either the current year or a range of years for copyright
     *
     * @return string
     */
    private function getCopyrightYear()
    {
        $startingYear = '2018';
        $currentYear = date('Y');
        $shortYear = date('y');

        if ($currentYear === $startingYear) {
            return $startingYear;
        } else {
            return $startingYear . '-' . $shortYear;
        }
    }
}
