<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolean
     */
    public function authorize()
    {
        /* TODO: Add form Authorization */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50',
            'slug' => 'required|max:100',
            'title_bg_image_url' => 'nullable|url',
            'lead_text' => 'required',
            'main_text' => 'nullable'
        ];
    }
}
