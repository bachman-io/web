<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolean
     */
    public function authorize()
    {
        /* TODO: Add form Authorization */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50',
            'slug' => 'required|max:100',
            'title_bg_image_url' => 'nullable|url',
            'synopsis' => 'required',
            'content_labels' => 'required'
        ];
    }
}
