<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;
use App\Story;
use Carbon\Carbon;

class StoriesController extends Controller
{

    private $story;
    private $genre;

    public function __construct(Story $story, Genre $genre)
    {
        $this->story = $story;
        $this->genre = $genre;
    }

    public function index()
    {
        $this->data['stories'] = $this->story->latestList();
        $this->data['genres'] = $this->genre->genreLinks();
        $this->data['latestChapter'] = $this->story->latestChapters($this->data['stories']);
        $this->data['labelString'] = $this->story->labelStrings($this->data['stories']);
        $this->data['wordCount'] = $this->story->wordCounts($this->data['stories']);
        $this->data['storyDates'] = $this->handleStoryListDates($this->data['stories']);
        $this->data['title'] = 'Stories';
        $this->data['description'] = 'A collection of stories long and short by Yours Truly';
        return view('stories.list', $this->data);
    }

    public function genre($id, $slug = null)
    {
        $this->data['genre'] = $this->genre->findOrFail($id);
        if ($slug === $this->data['genre']->slug) {
            $this->data['genres'] = $this->genre->genreLinks();
            $this->data ['stories'] = $this->genre->genreList($this->data['genre']->id);
            $this->data['latestChapter'] = $this->story->latestChapters($this->data['stories']);
            $this->data['labelString'] = $this->story->labelStrings($this->data['stories']);
            $this->data['wordCount'] = $this->story->wordCounts($this->data['stories']);
            $this->data['storyDates'] = $this->handleStoryListDates($this->data['stories']);
            $this->data['title'] = 'Stories - Genre: ' . $this->data['genre']->name;
            $this->data['description'] = 'Stories show a suspicious subset, specifically.';
            return view('stories.list', $this->data);
        }
    }

    public function book(
        $story_id,
        $chapter_id,
        $story_slug = null,
        $chapter_slug = null
    ) {
        $this->data['story'] = $this->story->getStory($story_id);

        if ($chapter_id == 0) {
            if ($story_slug === $this->data['story']->slug && $chapter_slug === 'home') {
                $this->data['story']->load(['labels',
                    'notes' => function ($query) {
                        $query->orderBy('created_at', 'desc');
                    }]);
                $this->data['title'] = $this->data['story']->title;
                $this->data['labelString'] = $this->story->labelString($this->data['story']);
                $this->data['wordCount'] = $this->data['story']->wordCount();
                $this->data['noteDates'] = $this->story->handleNoteDates($this->data['story']);
                $this->data['storyDates'] = $this->story->handleSingleStoryDates($this->data['story']);
                if (!$this->data['story']->published) {
                    return redirect()->route('stories.index', null, 302);
                }
                return view('stories.book.home', $this->data);
            }
            return redirect()->route('stories.book', ['story_id' => $story_id, 'chapter_id' => 0, 'story_slug' => $this->data['story']->slug, 'chapter_slug' => 'home']);
        } else {
            $this->data['currentChapter'] = $this->story->getChapter($this->data['story'], $chapter_id);
            $this->data['chapterDates'] = $this->story->handleChapterDates($this->data['currentChapter']);
            $this->data['markdown']['main'] = $this->convertMarkdown($this->data['currentChapter']->text);
            $this->data['title'] = $this->data['story']->title . ' - Chapter '
                . $this->data['currentChapter']->story_chapter_number
                . ': ' . $this->data['currentChapter']->title;
            if (!$this->data['currentChapter']->published) {
                return redirect()->route('stories.index', null, 302);
            }
            return view('stories.book.chapter', $this->data);
        }
    }

    private function handleStoryListDates($stories)
    {
        $storyDates = [];

        if (!empty($stories)) {
            foreach ($stories as $story) {
                $storyDates[$story->id]['created'] = $story->created_at->diffForHumans();
                $storyDates[$story->id]['updated'] = $story->updated_at->diffForHumans();

                if ($story->created_at->diffInDays() >= 7) {
                    $storyDates[$story->id]['created'] = $story->created_at->format('F jS');
                }
                if (Carbon::now()->format('Y') > $story->created_at->format('Y')) {
                    $storyDates[$story->id]['created'] = $story->created_at->format('n/j/Y');
                }

                if ($story->updated_at->diffInDays() >= 7) {
                    $storyDates[$story->id]['updated'] = $story->updated_at->format('F jS');
                }
                if (Carbon::now()->format('Y') > $story->updated_at->format('Y')) {
                    $storyDates[$story->id]['updated'] = $story->updated_at->format('n/j/Y');
                }
            }
        }

        return $storyDates;
    }
}
