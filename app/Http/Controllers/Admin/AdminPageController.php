<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class AdminPageController extends Controller
{

    public function index()
    {
        $this->data['title'] = 'Admin Home';
        return view('admin.index', $this->data);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
