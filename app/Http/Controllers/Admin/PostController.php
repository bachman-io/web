<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\StorePost;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{

    private $post;
    private $category;

    public function __construct(Post $post, Category $category)
    {
        $this->post = $post;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Post $post
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'Posts';
        $this->data = $this->post->allList($this->data);
        return view('admin.post.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = 'New Post';
        $this->data['categories'] = $this->category->alphabetic();
        return view('admin.post.new', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePost $request
     * @param Post      $post
     * @return \Illuminate\Http\Response
     */
    public function store(StorePost $request)
    {
        $this->post->addNew($request);
        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @param Post    $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['post'] = $this->post->findOrFail($id);
        $this->data['postDate'] = $this->post->handleSinglePostDate($this->data['post']);
        $this->data['markdown']['main'] = $this->convertMarkdown($this->data['post']->main_text);
        $this->data['title'] = $this->data['post']->title;
        return view('blog.post', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = 'Edit Post';
        $this->data['post'] = $this->post->findOrFail($id);
        $this->data['categories'] = $this->category->alphabetic();
        return view('admin.post.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer                  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePost $request, $id)
    {
        $this->post->updateExisting($id, $request);
        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
