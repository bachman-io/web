<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreLabel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Label;

class LabelController extends Controller
{

    private $label;

    public function __construct(Label $label)
    {
        $this->label = $label;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'Content Labels';
        $this->data['labels'] = $this->label->all();
        return view('admin.label.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = 'New Content Label';
        return view('admin.label.new', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLabel $request)
    {
        $this->label->addNew($request);
        return redirect()->route('label.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = 'Edit Content Label';
        $this->data['label'] = $this->label->findOrFail($id);
        return view('admin.label.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer                  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLabel $request, $id)
    {
        $this->label->updateExisting($id, $request);
        return redirect()->route('label.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
