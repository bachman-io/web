<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreAuthorNote;
use App\Story;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AuthorNote;

class AuthorNoteController extends Controller
{

    private $story;
    private $authorNote;

    public function __construct(Story $story, AuthorNote $authorNote)
    {
        $this->story = $story;
        $this->authorNote = $authorNote;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($story)
    {
        $this->data['story'] = $this->story
            ->with(['notes' => function ($query) {
                $query->orderBy('created_at', 'desc');
            }])
            ->findOrFail($story);
        $this->data['noteDates'] = $this->story
            ->handleNoteDates($this->data['story']);
        $this->data['title'] = $this->data['story']->title . ' - Author Notes';
        return view('admin.story.note.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($story)
    {
        $this->data['story'] = $this->story->findOrFail($story);
        $this->data['title'] = $this->data['story']->title . ' - New Author Note';
        return view('admin.story.note.new', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAuthorNote $request, $story_id)
    {
        $this->story->addAuthorNote($story_id, $request);
        return redirect()->route('note.index', ['story' => $story_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($story, $note)
    {
        $this->data['story'] = $this->story->findOrFail($story);
        $this->data['note'] = $this->authorNote->findOrFail($note);
        $this->data['title'] = $this->data['story']->title . ' - Edit Author Note';
        return view('admin.story.note.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer                  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAuthorNote $request, $story, $note)
    {
        $this->authorNote->updateExisting($note, $request);
        return redirect()->route('note.index', ['story' => $story]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
