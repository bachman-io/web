<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreChapter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Story;
use App\Chapter;
use Illuminate\Session\Store;

class ChapterController extends Controller
{

    private $story;
    private $chapter;

    public function __construct(Story $story, Chapter $chapter)
    {
        $this->story = $story;
        $this->chapter = $chapter;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($story)
    {
        $this->data['story'] = $this->story
            ->with(['chapters' => function ($query) {
                $query->orderBy('story_chapter_number');
            }])
            ->findOrFail($story);
        $this->data['title'] = $this->data['story']->title . ' - Chapters';
        return view('admin.story.chapter.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($story)
    {
        $this->data['story'] = $this->story
            ->withCount('chapters')
            ->findOrFail($story);
        $this->data['title'] = $this->data['story']->title . ' - New Chapter';
        return view('admin.story.chapter.new', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChapter $request, $story)
    {
        $this->story->addChapter($story, $request);
        return redirect()->route('chapter.index', ['story' => $story]);
    }

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($story, $chapter)
    {
        $this->data['story'] = $this->story->findOrFail($story);
        $this->data['chapter'] = $this->data['story']
            ->chapters()
            ->where('story_chapter_number', $chapter)
            ->firstOrFail();
        $this->data['title'] = $this->data['story']->title . ' - Edit Chapter';
        return view('admin.story.chapter.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer                  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreChapter $request, $story_id, $chapter_id)
    {
        $this->chapter->updateExisting($story_id, $chapter_id, $request);
        return redirect()->route('chapter.index', ['story' => $story_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
