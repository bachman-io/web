<?php

namespace App\Http\Controllers\Admin;

use App\Genre;
use App\Http\Requests\StoreStory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Story;
use App\Label;

class StoryController extends Controller
{

    private $story;
    private $genre;
    private $label;

    public function __construct(Story $story, Genre $genre, Label $label)
    {
        $this->story = $story;
        $this->genre = $genre;
        $this->label = $label;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['stories'] = $this->story->allList();
        $this->data['labelString'] = $this->story->labelStrings($this->data['stories']);
        $this->data['wordCount'] = $this->story->wordCounts($this->data['stories']);
        $this->data['title'] = 'Stories';
        return view('admin.story.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = 'New Story';
        $this->data['genres'] = $this->genre->alphabetic();
        $this->data['labels'] = $this->label->alphabetic();
        return view('admin.story.new', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStory $request)
    {
        $this->story->addNew($request);
        return redirect()->route('story.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = 'Edit Story Metadata';
        $this->data['story'] = $this->story->findOrFail($id);
        $this->data['genres'] = $this->genre->alphabetic();
        $this->data['labels'] = $this->label->alphabetic();
        $this->data['contentLabelIds'] = $this->story->contentLabelIds($this->data['story']);
        return view('admin.story.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  integer                  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreStory $request, $id)
    {
        $this->story->updateExisting($id, $request);
        return redirect()->route('story.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
