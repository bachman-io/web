<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use League\CommonMark\CommonMarkConverter;
use League\CommonMark\Environment;
use App\Markdown\Inline\Renderer\ImageRenderer as CustomImageRenderer;
use App\Markdown\Block\Renderer\ParagraphRenderer as CustomParagraphRenderer;

use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $data;

    protected function convertMarkdown($markdown)
    {

        $environment = Environment::createCommonMarkEnvironment();
        $environment->addInlineRenderer('League\CommonMark\Inline\Element\Image',
            new CustomImageRenderer);
        $environment->addBlockRenderer('League\CommonMark\Block\Element\Paragraph',
            new CustomParagraphRenderer);

        $converter = new CommonMarkConverter([], $environment);

        $html = $converter->convertToHtml($markdown);

        return $html;
    }

    protected function handlePostListDates($posts)
    {
        $postDates = [];

        if (!empty($posts)) {
            foreach ($posts as $post) {
                $postDates[$post->id]['created'] = $post->created_at->diffForHumans();
                $postDates[$post->id]['updated'] = $post->updated_at->diffForHumans();

                if ($post->created_at->diffInDays() >= 7) {
                    $postDates[$post->id]['created'] = $post->created_at->format('F jS');
                }
                if (Carbon::now()->format('Y') > $post->created_at->format('Y')) {
                    $postDates[$post->id]['created'] = $post->created_at->format('n/j/Y');
                }

                if ($post->updated_at->diffInDays() >= 7) {
                    $postDates[$post->id]['updated'] = $post->updated_at->format('F jS');
                }
                if (Carbon::now()->format('Y') > $post->updated_at->format('Y')) {
                    $postDates[$post->id]['updated'] = $post->updated_at->format('n/j/Y');
                }
            }
        }

        return $postDates;
    }

    protected function handlePostListMarkdown($posts)
    {
        $markdown = [];

        foreach ($posts as $post) {
            $markdown[$post->id] = $this->convertMarkdown($post->lead_text);
        }

        return $markdown;
    }
}
