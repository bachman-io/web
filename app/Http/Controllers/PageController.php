<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Post;

class PageController extends Controller
{
    public function home(Post $post, Category $category)
    {
        $this->data = $post->archiveLinks($this->data);
        $this->data = $category->categoryLinks($this->data);
        $this->data = $post->latestList($this->data);
        $this->data['postDates'] = $this->handlePostListDates($this->data['posts']);
        $this->data['markdown'] = $this->handlePostListMarkdown($this->data['posts']);

        $this->data['title'] = 'Home';
        $this->data['description'] = 'Some guy on the internet writes some blog posts.';

        return view('home', $this->data);
    }
}
