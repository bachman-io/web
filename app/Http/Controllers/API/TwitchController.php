<?php

namespace App\Http\Controllers\API;

use GuzzleHttp\Exception\ClientException;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Illuminate\Cache\Repository as Cache;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Strategy\GreedyCacheStrategy;
use Kevinrob\GuzzleCache\Storage\LaravelCacheStorage;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\RequestException;
use App\Http\Controllers\Controller;

class TwitchController extends Controller
{
    private $client;
    private $cache;

    public function __construct(Cache $cache)
    {
        $stack = HandlerStack::create();
        $stack->push(
            new CacheMiddleware(
                new GreedyCacheStrategy(
                    new LaravelCacheStorage($cache),
                    120
                )
            ),
            'twitch_cache'
        );
        $this->client = new Client([
            'base_uri' => 'https://api.twitch.tv/helix/',
            'handler' => $stack
        ]);

        $this->cache = $cache;
    }

    public function index()
    {
        $response = $this->client->request('GET', 'streams', [
            RequestOptions::HEADERS => [
                'Client-ID' => env('TWITCH_CLIENT_ID')
            ],
            RequestOptions::QUERY => [
                'user_login' => env('TWITCH_NAME', 'bachmac')
            ]
        ]);
        $json = json_decode($response->getBody());
        return response()->json($json);
    }
}
