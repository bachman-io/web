<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use League\CommonMark\CommonMarkConverter;
use App\Post;
use App\Category;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{

    private $post;
    private $category;

    public function __construct(Post $post, Category $category)
    {
        $this->post = $post;
        $this->category = $category;
        $this->data = $this->post->archiveLinks($this->data);
        $this->data = $this->category->categoryLinks($this->data);
    }

    public function index()
    {
        return redirect()->to('/');
    }

    public function post($id, $slug = null)
    {
        $this->data['post'] = $this->post->findOrFail($id);

        if ($slug === $this->data['post']->slug) {
            if ($this->data['post']->published === 0) {
                return redirect()->route('blog.index', null, 302);
            }
            $this->data['postDates'] = $this->post->handleSinglePostDates($this->data['post']);
            $this->data['markdown']['main'] = $this->convertMarkdown($this->data['post']->main_text);
            $this->data['title'] = $this->data['post']->title;
            $this->data['description'] = $this->data['post']->lead_text;
            return view('blog.post', $this->data);
        }

        return redirect()->route('blog.post', ['id' => $id, 'slug' => $this->data['post']->slug]);
    }

    public function category($id, $slug = null)
    {
        $this->data['category'] = $this->category->findOrFail($id);
        if ($slug === $this->data['category']->slug) {
            $this->data ['posts'] = $this->post->categoryList($this->data['category']->id);
            $this->data['postDates'] = $this->handlePostListDates($this->data['posts']);
            $this->data['markdown'] = $this->handlePostListMarkdown($this->data['posts']);
            $this->data['title'] = 'Category: ' . $this->data['category']->name;
            $this->data['description'] = 'Posts possessing a peculiar property, perhaps';
            return view('blog.list', $this->data);
        }

        return redirect()->route('blog.category', ['id' => $id, 'slug' => $this->data['category']->slug]);
    }

    public function archive($year, $month)
    {
        $this->data['posts'] = $this->post->findByDate($year, $month);
        $this->data['postDates'] = $this->handlePostListDates($this->data['posts']);
        $this->data['markdown'] = $this->handlePostListMarkdown($this->data['posts']);

        $this->data['posts'] = $this->post->findByDate($year, $month);
        $this->data['postDate'] = $this->handlePostListDates($this->data['posts']);
        $this->data['markdown'] = $this->handlePostListMarkdown($this->data['posts']);

        $months = [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => "December"
        ];

        $this->data['title'] = 'Archive: ' . $months[$month] . ' ' . $year;
        $this->data['description'] = 'Those were the good old days...';

        return view('blog.list', $this->data);
    }
}
