<?php

namespace App;

use App\Http\Requests\StoreChapter;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Chapter extends Model
{

    protected $fillable = [
        'story_chapter_number',
        'title',
        'slug',
        'published',
        'text',
        'word_count'
    ];

    public function story()
    {
        return $this->belongsTo('App\Story');
    }

    public function updateExisting($story_id, $chapter_id, StoreChapter $request)
    {
        $chapter = $this->where([
            ['story_id', '=', $story_id],
            ['story_chapter_number', '=', $chapter_id]
        ])->first();

        $chapter->title = $request->title;
        $chapter->slug = $request->slug;
        $chapter->text = $request->text;
        $chapter->word_count = $request->word_count;

        if (isset($request->published)) {
            if ($chapter->published === 0) {
                $chapter->created_at = Carbon::now();
            }
            $chapter->published = $request->published;
        } else {
            $chapter->published = 0;
        }

        $chapter->save();
    }
}
