<?php

namespace App;

use App\Http\Requests\StorePost;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Post extends Model
{

    protected $fillable = ['title',
        'slug',
        'published',
        'nsfw',
        'title_color',
        'title_bg_color',
        'title_bg_image_url',
        'author_id',
        'lead_text',
        'main_text'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function author()
    {
        return $this->belongsTo('App\User');
    }

    public function addNew(StorePost $request)
    {
        $this->title = $request->title;
        $this->slug = $request->slug;
        $this->type = $request->type;
        if (isset($request->published)) {
            $this->published = $request->published;
        }
        if (isset($request->nsfw)) {
            $this->nsfw = $request->nsfw;
        }
        $this->title_bg_image_url = $request->title_bg_image_url;
        $this->author_id = $request->author_id;
        $this->category_id = $request->category_id;
        $this->lead_text = $request->lead_text;
        $this->main_text = $request->main_text;
        $this->save();
    }

    public function updateExisting($id, StorePost $request)
    {
        $post = $this->findOrFail($id);
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->type = $request->type;
        if (isset($request->published)) {
            if ($post->published === 0) {
                $post->created_at = Carbon::now();
            }
            $post->published = $request->published;
        } else {
            $post->published = 0;
        }
        if (isset($request->nsfw)) {
            $post->nsfw = $request->nsfw;
        } else {
            $post->nsfw = 0;
        }
        $post->title_bg_image_url = $request->title_bg_image_url;
        $post->author_id = $request->author_id;
        $post->category_id = $request->category_id;
        $post->lead_text = $request->lead_text;
        $post->main_text = $request->main_text;
        $post->save();
    }

    public function latestOne()
    {
        return $this->where('published', 1)->orderBy('created_at', 'desc')
            ->first();
    }

    public function latestList($data)
    {
        $data['posts'] = $this->where('published', 1)->orderBy('created_at', 'desc')
            ->paginate(10);

        return $data;
    }

    public function allList($data)
    {
        $data['posts'] = $this->orderBy('created_at', 'desc')
            ->paginate(10);

        return $data;
    }

    public function categoryList($id)
    {
        $posts = $this->where([
            ['category_id', '=', $id],
            ['published', '=', 1]])
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return $posts;
    }

    public static function archiveLinks($data)
    {
        $data['archive_years'] = DB::table('posts')->select(
            DB::raw('YEAR(created_at) year')
        )
            ->where('published', 1)
            ->groupBy('year')
            ->orderBy('year', 'desc')
            ->distinct()
            ->get();
        $data['archive_months'] = [];
        if (!empty($data['archive_years'])) {
            foreach ($data['archive_years'] as $y) {
                $data['archive_months'][$y->year] = DB::table('posts')
                    ->select(DB::raw('MONTH(created_at) month,
                    MONTHNAME(created_at) month_name,
                    COUNT(*) posts_count'))
                    ->where([
                        [DB::raw('YEAR(created_at)'), '=', $y->year],
                        ['published', '=', 1]
                    ])
                    ->groupBy(['month', 'month_name'])
                    ->orderBy('month', 'desc')
                    ->distinct()
                    ->get();
            }
        }

        return $data;
    }

    public function handleSinglePostDates($post)
    {
        $postDates['created'] = $post->created_at->diffForHumans();
        $postDates['updated'] = $post->updated_at->diffForHumans();

        if ($post->created_at->diffInDays() >= 7) {
            $postDates['created'] = $post->created_at->format('F jS');
        }
        if (Carbon::now()->format('Y') > $post->created_at->format('Y')) {
            $postDates['created'] = $post->created_at->format('n/j/Y');
        }

        if ($post->updated_at->diffInDays() >= 7) {
            $postDates['updated'] = $post->updated_at->format('F jS');
        }
        if (Carbon::now()->format('Y') > $post->updated_at->format('Y')) {
            $postDates['updated'] = $post->updated_at->format('n/j/Y');
        }


        return $postDates;
    }

    public function findByDate($year, $month)
    {
        return $this->where([
            [DB::raw('YEAR(created_at)'), '=', $year],
            [DB::raw('MONTH(created_at)'), '=', $month],
            ['published', '=', 1]
        ])->orderBy('created_at', 'desc')
            ->paginate(10);
    }
}
