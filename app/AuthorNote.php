<?php

namespace App;

use App\Http\Requests\StoreAuthorNote;
use Illuminate\Database\Eloquent\Model;

class AuthorNote extends Model
{

    /**
     * Fillable fields when mass-assigning the Model
     *
     * @var array
     */
    protected $fillable = ['text'];

    /**
     * Update an existing author note
     *
     * @param $id
     * @param StoreAuthorNote $request
     */
    public function updateExisting($id, StoreAuthorNote $request)
    {
        $note = $this->find($id);
        $note->text = $request->text;
        $note->save();
    }
}
